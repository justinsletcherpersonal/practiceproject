import MainCard from "./components/MainCard";
import "./styles/style.css";

function App() {
  return (
    <div className="box">
      <MainCard userName="liam" />
      <MainCard userName="justin" />
      <MainCard userName="luke" />
      <MainCard userName="louis" />
    </div>
  );
}

export default App;
