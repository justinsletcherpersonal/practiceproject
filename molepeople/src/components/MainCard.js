import React from "react";
import "../styles/MainCard.css";

const MainCard = (props) => {
  return (
    <div className={"card " + props.userName}>
      <h1>{props.userName.toUpperCase()}</h1>
      <img src="https://imgs.callofduty.com/content/dam/atvi/callofduty/cod-touchui/warzone/strategy-guide/articles/2021/chapter-2/02d-operators/02D-Operators-001-E.jpg"></img>
    </div>
  );
};

export default MainCard;
